"""Written by Marius and Alexander"""

# Imports only allow: numpy, scipy and matplotlib
import numpy as np
from scipy.sparse.linalg import eigs
import matplotlib.pyplot as plt


class PCA:
    """Principle Component Analysis class for linear dimensional reduction"""

    def __init__(self, file, dimension):
        self.file = np.genfromtxt(file, delimiter=',')
        self.dim_in = dimension
        self.dim_out = 2
        # Centering / de-meaning / normalize
        self.centered_data = self.file - self.file.mean(axis=0)

    def fit(self):
        """Standardizing: Fit the PCA model to the given data points"""
        # Find the direction of maximal variances:
        # - numpy.cov calculates the covariance matrix sigma
        sigma = np.cov(self.file.T) # T is transpose

        # - The direction of the eigenvector which corresponds
        # to the largest eigenvalues of the covariance matrix
        if (self.dim_in - 1) > self.dim_out:
            # find largest eigenvalues and corresponding eigenvector
            eigenvalues, eigenvectors = eigs(sigma, k=self.dim_out) # Gives error
        elif (self.dim_in - 1) == self.dim_out:
            # find all eigenvalues and eigenvectors
            eigenvalues, eigenvectors = np.linalg.eigh(sigma)
            index = np.argsort(eigenvalues)[::-1]
            eigenvectors = eigenvectors[:, index[0:self.dim_in]]
        return np.real(eigenvectors) # real ignores the imaginary part of the vector

    def transform(self, colors):
        """Transforms the data"""

        if len(colors) == 0:
            colors = np.arange(self.file.shape[0])

        final_points = np.matmul(self.centered_data, self.fit())
        plt.scatter(final_points[:, 0], final_points[:, 1], s=10, c=colors, marker='.', cmap='jet')
        plt.show()


def main():
    """Main function"""
    pca = PCA("swiss_data.csv", 3)
    # pca = PCA("digits.csv", 64)
    eigenvectors = pca.fit()
    print(eigenvectors)
    pca.transform([])


if __name__ == '__main__':
    main()
