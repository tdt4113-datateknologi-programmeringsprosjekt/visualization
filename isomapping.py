"""Uses isomapping to visualize a 3d structor to 2d"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from sklearn.utils.graph_shortest_path import graph_shortest_path


class Isomapping:
    """Methods to use isomapping, (runs in a "main" function)"""

    @staticmethod
    def read(filename):
        """
        Reads a cvc file
        :param filename: file to read
        :return: numpy.ndarray
        """
        read_file = np.genfromtxt(filename, delimiter=",")
        print(type(read_file))
        return read_file

    # ER NØDT TIL Å BRUKE CDIST ELLERS ER ARRAY TOO BIG
    @staticmethod
    def pairwise_euclidean_distance(data):
        """
        Finds the pairwise euclidean distance from a given numpy.ndarray
        :param data: numpy.ndarray to use
        :return: pairwise distances (Used in isomap to find closest nodes
        """
        pairwise_distances = scipy.spatial.distance.cdist(data, data, metric="euclidean")
        return pairwise_distances

    @staticmethod
    def plot_swiss(data, title):
        """
        Plots the dots for the swiss data (different method to get colors)
        :param data: coordinates to plot (X,Y)
        :param title: Title for the "graph"
        :return: a "graph" with the nodes
        """
        fig = plt.figure()
        fig.set_size_inches(10, 10)
        a_x = fig.add_subplot(111)
        a_x.set_title(title)
        a_x.set_xlabel('Component: 1')
        a_x.set_ylabel('Component: 2')
        color = np.arange(len(data))

        a_x.scatter(data[:, 0], data[:, 1], marker='.', alpha=0.7, c=color)
        a_x.set_ylabel('Y')
        a_x.set_xlabel('X')
        plt.show()

    @staticmethod
    def plot_digits(data, title):
        """
        Plots the dots for the digits data
        :param data: coordinates to plot (X,Y)
        :param title: Title for the "graph"
        :return: a "graph" with the nodes
        """
        fig = plt.figure()
        fig.set_size_inches(10, 10)
        a_x = fig.add_subplot(111)
        a_x.set_title(title)
        a_x.set_xlabel('Component: 1')
        a_x.set_ylabel('Component: 2')
        color = np.genfromtxt("digits_label.csv", delimiter=',')
        a_x.scatter(data[:, 0], data[:, 1], marker='.', alpha=0.7, c=color)
        a_x.set_ylabel('Y')
        a_x.set_xlabel('X')
        plt.show()


def mds(data):
    """
    Complicated Calculation to compute the points in a 2d space
    spared = Shortest path^2 (squared proximity matrix)
    points = number of points per row
    I = identity matrix
    J = centring matrix
    B = double centring
    eigenval = eigenvalues
    eigenvec = eigenvector
    we will find the 2 largest eigenval, eigenvec
    diagonal = diagonal matrix of eigenval
    ms_matrix = the matrix mds returns
    :param D: Data from shortest graph path
    :return: calculated matrix in 2d (X, Y)
    """

    squared = data*data
    points = data.shape[0]
    math_i = np.identity(points)
    math_j = np.ones((points, 1))
    math_j = math_i - (1/points)*(math_j@math_j.transpose())
    math_b = (-1 / 2)*math_j@squared@math_j  # B = -(1/2).JD²J

    eigenval, eigenvec = scipy.sparse.linalg.eigs(math_b, k=2)
    eigenval = eigenval.real
    eigenvec = eigenvec.real
    diagonal = np.diag(eigenval)**(1/2)
    ms_matrix = eigenvec@diagonal
    return ms_matrix


def isomap():
    """"Main function to use all the other metodes in Isomapping
    k = k neighbours to the node
    data = data from file
    line 129-134 finds the shortest path from the k closest neighbour nodes
    """
    k = 30
    iso = Isomapping()

    # LES DATA
    data = iso.read("swiss_data.csv")

    ndata = np.shape(data)[0]

    # Finn parvis avstand
    pairwise_d = iso.pairwise_euclidean_distance(data)

    # Sorterer array
    indencies = pairwise_d.argsort()

    # Finner alle som ikke er blandt de nærmeste og setter det til 0
    neighbours = indencies[:, :k+1]
    closest = np.ones((ndata, ndata), dtype=float) * 0
    for i in range(ndata):
        closest[i, neighbours[i, :]] = pairwise_d[i, neighbours[i, :]]
    # Finner korteste vei mellom nodene, fjerner nuller
    shortest = graph_shortest_path(closest)
    print(shortest)

    # Forsøk på MDS
    x_y = mds(shortest)

    # Plotter
    iso.plot_swiss(x_y, "swiss")


if __name__ == '__main__':
    isomap()
