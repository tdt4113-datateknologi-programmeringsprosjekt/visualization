"""Docstring"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial

class TSNE:
    """help methods to use in main"""

    def __init__(self, filename, iterations, k_nearest=20):
        self.data = np.genfromtxt(filename, delimiter=',')
        self.iterations = iterations
        self.k_nearest = k_nearest

    def pairwise_euclidian_distance(self, data):
        """
        Finds the pairwise distance
        :param data: data from file
        :return: pairwise distance
        """
        pdist = scipy.spatial.distance.pdist(data, 'euclidean')
        return scipy.spatial.distance.squareform(pdist)

    def knn(self):
        """
        finds the k closest nodes and fills in 0's or 1's depending on
        if the element close or not
        :return: k closest nodes
        """
        data = self.pairwise_euclidian_distance(self.data)
        data_shape = data.shape[0]
        for row in range(data_shape):
            argpart = np.argpartition(data[row], self.k_nearest)
            k_last = argpart[:self.k_nearest]
            smallest_elems = data[row][k_last]
            k_small = max(smallest_elems)
            for elem in range(len(data[row])):
                data[row][elem] = 0 if data[row][elem] > k_small else 1
        return data

    def calc_q(self, values):
        """
        Calculates q, which we use for calculations
        :param values: y_values, randomly calculated based on length
        :return: q
        """
        distances = self.pairwise_euclidian_distance(values)
        q_value = 1 / (1 + distances)
        np.fill_diagonal(q_value, 0)
        return q_value

    def gradient_calc(self, p_matrix, q_matrix, y_val, dist_matrix):
        """
        Help function to chose the gradient
        :param p_matrix: probability matrix
        :param q_matrix: normalized distance
        :param y_val: y_val
        :param dist_matrix: distance matrix
        :return: which gradient
        """
        expanded = (p_matrix - q_matrix) * dist_matrix
        difference_y = np.expand_dims(y_val, 1) - np.expand_dims(y_val, 0)
        gradient = 4 * np.einsum('ij, ijk -> ik', expanded, difference_y)
        return gradient

def plot_coordinates(array):
    """
    Helps us to plot the coordinates
    :param array: array to plot
    :return: nothing. just a graph
    """
    labels = np.loadtxt('digits_label.csv')
    plt.figure(figsize=(8, 8))
    plt.gca().axis('equal')
    plt.scatter(array[:, 0], array[:, 1], s=10, c=labels, marker='.', cmap='jet')
    plt.show()


def main():
    """
    main function to run the tSNE with the tsne class
    :return: a runnable "program"
    """
    tsne = TSNE('digits.csv', iterations=200)
    distance = tsne.pairwise_euclidian_distance(tsne.data)
    len_elements = np.shape(distance)[0]
    k_nearest = np.sqrt(tsne.knn())
    sign_p_matrix = np.sign(k_nearest)
    np.fill_diagonal(sign_p_matrix, 0)
    p_matrix = sign_p_matrix / np.sum(sign_p_matrix)
    p_matrix_cheat = 4 * p_matrix
    y_values = np.random.normal(0, 10 ** (-4), (len_elements, 2))
    gain = np.ones(y_values.shape)
    change = np.zeros(y_values.shape)
    epsilon = 500.

    for i in range(1, tsne.iterations + 1):
        print(str(i) + '/' + str(tsne.iterations))
        dist_matrix = tsne.calc_q(y_values)
        normalize_dist = dist_matrix / (np.sum(dist_matrix))
        new_p_matrix = p_matrix_cheat if i < 100 else p_matrix
        gradients = tsne.gradient_calc(new_p_matrix, normalize_dist, y_values, dist_matrix)
        gain = np.where(np.sign(change) != np.sign(gradients), gain + 0.2, gain * 0.8)
        new_alpha = 0.5 if i < 150 else 0.8
        change *= new_alpha
        change -= gain * gradients * epsilon

        y_values += change

    plot_coordinates(y_values)


if __name__ == '__main__':
    main()
